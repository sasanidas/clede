;;; test-clede.el --- CLEDE integration test file.               -*- lexical-binding: t; -*-

;; Copyright (C) 20 Fermin Munoz

;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;; The requires
(require 'ert)
(require 'ert-x)
(require 'clede)
(require 'sly)
(require 'clede-inf-lisp)
(require 'inf-lisp)


;;; CLEDE Lisp interface

;; From https://github.com/joaotavora/sly/blob/master/test/sly-mrepl-tests.el
(cl-defun sly-mrepl-tests--assert-prompt (&optional (prompt "CL-USER>"))
  (let ((proper-prompt-p nil))
    (cl-loop 
     repeat 5
     when (looking-back (format "%s $" prompt) (- (point) 100))
     do (setq proper-prompt-p t)
     (cl-return)
     do (sit-for 0.3))
    (or proper-prompt-p
        (ert-fail (format "Proper prompt not seen in time (saw last 20 chars as \"%s\")"
                          (buffer-substring-no-properties (max (point-min)
                                                               (- (point-max)
                                                                  20))
                                                          (point-max)))))))

(defun sly-mrepl-tests--assert-dedicated-stream ()
  (let ((dedicated-stream nil))
    (cl-loop 
     repeat 5
     when (and sly-mrepl--dedicated-stream
               (processp sly-mrepl--dedicated-stream)
               (process-live-p sly-mrepl--dedicated-stream))
     do (setq dedicated-stream t)
     (cl-return)
     do (sleep-for 0 300))
    (or dedicated-stream
        (ert-fail "Dedicated stream not setup correctly"))))

(defvar sly-mrepl-tests--debug nil)
(setq sly-mrepl-tests--debug nil)

(defmacro test-clede-integrations-sly-repl-setup (&rest body)
  (declare (debug (&rest form)))
  `(let ((sly-buffer-package "COMMON-LISP-USER"))
     (with-current-buffer (sly-mrepl-new (sly-current-connection)
                                         "test-only-repl")
       (unwind-protect
           (progn
             (sly-mrepl-tests--assert-prompt)
             (sly-mrepl-tests--assert-dedicated-stream)
             ,@body)
         (unless sly-mrepl-tests--debug
           (kill-buffer (current-buffer)))))))


(ert-deftest  clede-lisp-interface-send-sexp-test ()
  (let* ((test-buffer (get-buffer-create "*CLEDETEST*")))
    (with-current-buffer test-buffer
      (test-clede-integrations-sly-repl-setup
       (let ((clede-lisp-interface-package 'sly))
	 (should (eq (clede-lisp-interface-send-sexp (+ 4 5)) 9))))

      (let ((clede-lisp-interface-package 'inf-lisp))
	(inferior-lisp "sbcl")
	(sleep-for 0.2)
	(should (eq (clede-lisp-interface-send-sexp (+ 4 5)) 9))
	(delete-process (get-buffer-process "*inferior-lisp*"))
	(kill-buffer "*inferior-lisp*")))
    (kill-buffer test-buffer)))




(ert t)



(provide 'test-clede-integrations.el)
;;; test-clede-integrations.el ends here
