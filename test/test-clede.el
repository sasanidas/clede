;;; test-clede.el --- clede test file.               -*- lexical-binding: t; -*-

;; Copyright (C) 20 Fermin Munoz

;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;; The requires
(require 'ert)
(require 'clede)
(require 'find-lisp)


;;; CLEDE main file tests
(ert-deftest clede-libraries-integrations-test ()
  "Make sure that all the libraries are loaded correctly."
  (clede-start)
  (should-not
   (member nil
	   (mapcar #'featurep clede-integrations))))

(ert-deftest clede-load-path-test ()
  "Make sure that all the directories are loaded correctly."
  (clede-start)
  (should-not
   (member nil
	   (mapcar (lambda(file)
		     (featurep (intern file)))
		   (mapcar #'file-name-base
			   (seq-filter (lambda (file)
					 (string= (file-name-extension file) "el"))
				       (find-lisp-find-files ".." "^clede")))))))


(ert t)



(provide 'test-clede.el)
;;; test-clede.el ends here
