;;; clede-refactor.el ---  CLEDE refactoring utilities            -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 13 Mar 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/clede
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires

(require 'cl-lib)
(require 'syntax)
(require 'imenu)
(require 'seq)
(require 'replace)
(require 'clede-lisp-interface)
(require 'xref)

(defcustom clede-refactor-interactive t
  "Whether or not select each refactor symbol manually."
  :type 'boolean
  :group 'clede)

(defun clede-refactor--create-regexp (symbol)
  "Create SYMBOL exclusive regexp."
  (format "\\_<\\(%s\\)\\_>" (regexp-quote symbol)))

(defun clede-refactor--in-string-or-comment (p)
  "Return non-nil if P is within a string or comment."
  (let ((ppss (syntax-ppss p)))
    (or (car (setq ppss (nthcdr 3 ppss)))
        (car (setq ppss (cdr ppss)))
        (nth 3 ppss))))

(defun clede-refactor--tag-tail ()
  "Return the tag tail."
  (semantic-tag-end (semantic-current-tag)))

(defun clede-refactor--tag-position ()
  "Get the root function position of the closest tag."
  (semantic-tag-start (semantic-current-tag)))

(defun clede-refactor-rename-region (symb new-symbol beg end &optional regex)
  "Search in the region from BEG to END the references of SYMB.
Changing the match with NEW-SYMBOL."
  (let* ((regexp (or regex (clede-refactor--create-regexp symb))))
    (save-excursion
      (goto-char beg)
      (while (and (re-search-forward regexp nil t)
		  (<= (point) end))
	(let ((target (match-data)))
	  (when (not (clede-refactor--in-string-or-comment (point)))
	    (unwind-protect
		(when (y-or-n-p "Rename? ")
		  (set-match-data target)
		  (replace-match new-symbol t nil nil 0)))))))))

(defun clede-refactor-replace-symbol-region (old-symbol new-symbol)
  "Replace in active region OLD-SYMBOL with NEW-SYMBOL."
  (interactive "sSymbol to replace: \nsNew symbol: ")
  (let* ((r-begin (region-beginning))
	 (r-end (region-end)))
    (clede-refactor-rename-region old-symbol new-symbol r-begin r-end)))

(defun clede-refactor-replace-symbol-tag (new-symbol)
  "Replace the symbol at `point' with NEW-SYMBOL."
  (interactive "sReplace with: ")
  (let* ((current-symbol (thing-at-point 'symbol t))
	 (tag-header (clede-refactor--tag-position))
	 (tag-tail (clede-refactor--tag-tail)))
    (clede-refactor-rename-region current-symbol new-symbol tag-header tag-tail)))

(cl-defun clede-refactor--get-references (symbol type)
  "Get the references of SYMBOL of TYPE.
For now, only `sly' is supported."
  (if (fboundp 'sly)
      (sly-eval
       `(slynk:xref ',type ',symbol))
    (error "This commands requires `sly' to work")))

(defun clede-refactor--get-definition (symbol)
  "Get definitions of SYMBOL.
For now, only `sly' is supported."
  (if (fboundp 'sly)
      (sly-eval `(slynk:find-definitions-for-emacs ',symbol))
    (error "This commands requires `sly' to work")))

(cl-defun clede-refactor--clean-references (references)
  "Clean the x-ref list of REFERENCES to a more simple version."
  (cl-loop for ref in (reverse references)
	   for locations = (cdr ref)
	   nconc
	   (cl-loop for location in locations
		    collect
		    (cl-destructuring-bind (_
					    (_ file)
					    (_ pos)
					    (_ _))
			location
		      (cons file pos)))))

(cl-defun clede-refactor--refactor-symbol (&key position new old)
  "Replace symbol OLD for NEW starting in POSITION.
POSITION is a cons of (file . char-position)."
  (cl-destructuring-bind (file . pos)
      position
    (find-file file)
    (with-current-buffer (current-buffer)
      (goto-char pos)
      (let ((bounds (thing-at-point-bounds-of-list-at-point)))
	(clede-refactor-rename-region old new (car bounds) (cdr bounds)
			(rx (or (literal old) (group word (or ":" "::") (literal old)))))))))

(cl-defun clede-refactor--symbols (old new references)
  "Loop over REFERENCES changing OLD to NEW."
  (push
   (cons (buffer-file-name (current-buffer))
	 (car (thing-at-point-bounds-of-list-at-point)))
   references)
  (save-mark-and-excursion
    (cl-loop for pos in references
	     do (clede-refactor--refactor-symbol
		 :position pos
		 :new new
		 :old old))))

(cl-defun clede-refactor--at-point (new-symbol &key type)
  "Refactor symbol at point to NEW-SYMBOL of type TYPE."
  (let ((current-symbol (thing-at-point 'symbol t)))
    (clede-refactor--symbols current-symbol new-symbol
	       (nconc (clede-refactor--clean-references
		       (clede-refactor--get-definition (format "%s" current-symbol)))
		      (clede-refactor--clean-references
		       (clede-refactor--get-references (format "%s" current-symbol) type))))))

(defun clede-refactor-variable (new-symbol)
  "Refactor the references of variable at point with NEW-SYMBOL."
  (interactive "sRefactor to: ")
  (clede-refactor--at-point new-symbol :type :references))

(defun clede-refactor-function (new-symbol)
  "Refactor the references of function at point with NEW-SYMBOL."
  (interactive "sRefactor to: ")
  (clede-refactor--at-point new-symbol :type :calls))


(provide 'clede-refactor)
;;; clede-refactor.el ends here
