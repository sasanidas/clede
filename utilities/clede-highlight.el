;;; clede-highlight.el ---  CLEDE highlight utilities            -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 13 Mar 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/clede
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires

(require 'cl-lib)
(require 'imenu)
(require 'clede-refactor)
(require 'clede-semantic-util)

(defvar clede-highlight-timer nil)

(defface clede-highlight-face
  '((t (:inherit match)))
  "Face for highlighting of matches."
  :group 'clede)

(defvar clede-highlight-face 'clede-highlight-face)

(defvar clede-highlight--overlays nil)
(make-variable-buffer-local 'clede-highlight--overlays)


(cl-defun clede-highlight---get-var-locations (current-symbol)
  "Get all the positions of the CURRENT-SYMBOL and return a list of it."
  (let* ((let-context (clede-semantic-util-get-let-context
		       current-symbol))
	 (regexp (clede-refactor--create-regexp current-symbol))
	 (local-positions))
    (when (car let-context)
      (save-excursion
	(goto-char (car let-context))
	(while (and (re-search-forward regexp nil t)
		    (<= (point) (cdr let-context)))
	  (when (not (clede-refactor--in-string-or-comment (point)))
	    (if local-positions
		(push (match-beginning 1) local-positions)
	      (setq local-positions (list (match-beginning 1)))))))
      local-positions)))


(cl-defun clede-highlight--disable-hl ()
  "Disable highlight."
  (dolist (ov (overlays-in (point-min) (point-max)))
    (when (overlay-get ov 'clede-overlay-p)
      (delete-overlay ov))))

(defun clede-highlight-dehighlight-after-change (_ __ ___)
  "Help function to disable hl."
  (ignore-errors
    (clede-highlight--disable-hl)))

(defvar clede-hl--overlays)
(cl-defun clede-highlight--start ()
  "Start highlight local variables."
  (let* ((tap (thing-at-point 'symbol t))
	 (local-positions (if tap (clede-highlight---get-var-locations tap) nil)))
    (save-excursion
      (when (and local-positions
		 (not (zerop (length local-positions))))
	(add-to-list 'after-change-functions 'clede-highlight-dehighlight-after-change)
	(mapc (lambda (position)
		(goto-char position)
		(let* ((tap (bounds-of-thing-at-point 'symbol))
		       (ov (make-overlay (car tap)  (cdr tap))))
		  (overlay-put ov 'priority 1) ;; few value
		  (overlay-put ov 'face clede-highlight-face)
		  (overlay-put ov 'clede-overlay-p t)
		  (setq clede-hl--overlays (cons ov clede-highlight--overlays))))
	      local-positions)))))

(cl-defun clede-highlight--timer-stop ()
  "Stop the timer."
  (when (timerp clede-highlight-timer)
    (cancel-timer clede-highlight-timer))
  (setq clede-highlight-timer nil))

(cl-defun clede-highlight--timer-start ()
  "Start the timer."
  (if clede-highlight-timer
      (memq clede-highlight-timer timer-idle-list)
    (setq clede-highlight-timer
	  (run-with-idle-timer idle-update-delay t #'clede-highlight--start))))

(define-minor-mode clede-highlight-minor-mode
  "Toggle highlight mode on or off.
In highlight mode, the highlight the current symbol if recognize
as a local variable."
  :group 'clede
  (cond
   ((bound-and-true-p clede-highlight-minor-mode)
    (clede-highlight--timer-start))
   (t
    (clede-highlight--timer-stop)
    (delete 'clede-highlight-dehighlight-after-change after-change-functions))))


;;;###autoload
(define-globalized-minor-mode global-clede-highlight-minor-mode clede-highlight-minor-mode clede-highlight-minor-mode
  :group 'clede)

(provide 'clede-highlight)
;;; clede-highlight.el ends here
