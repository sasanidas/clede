;;; clede-commands.el ---  CLEDE commands integration            -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 22 May 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/clede
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires

(require 'cl-lib)
(require 'clede-lisp-interface)

(defcustom clede-commands-list nil
  "List of commands defined by the user.
The syntax for each command is a cons with the name of the
command and the command (as a string) that is going to be send to
the default REPL interface defined in `clede-lisp-interface-package'."
  :group 'clede
  :type 'list)


(cl-defun clede-commands-run ()
  "Run a command from `clede-commands-list'."
  (interactive)
  (let ((command
	 (cdr (assoc (completing-read "Command: "
				      clede-commands-list nil t)
		     clede-commands-list))))
    (clede-lisp-interface-send-repl-command command)))



(provide 'clede-commands)
;;; clede-commands.el ends here
