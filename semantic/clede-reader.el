;;; clede-reader.el ---  CLEDE CL reader implementation           -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 17 Mar 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/clede
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; DEFINING CUSTOM READER SYNTAX
;; -----------------------------
;;
;; Example of defining custom syntax (note this function uses
;; `clede-reader-read-list', which see, to get a list of Lisp datums):
;;
;;     (def-readclede-reader-syntax ?{
;;         (lambda (in ch)
;;           (let ((list (clede-reader-read-list in ?} t)))
;;             `(list ,@(cl-loop for (key val) on list by #'cddr
;;                               collect `(cons ,key ,val))))))
;;
;; and now you can type into the REPL:
;;
;;     ELISP> { :foo 1 :bar "string" :baz (+ 2 3) }
;;     ((:foo . 1)
;;      (:bar . "string")
;;      (:baz . 5))
;;
;; [1] https://common-lisp.net/project/named-readtables/
;;

;;; Code:

(defvar clede-reader-var-read  (symbol-function #'read)
  "Remember the original `read' function.
Because we'll have to use it in some situations that can't be
handled from Lisp code.")

(defvar clede-reader-macro-chars (make-hash-table :test 'eq)
  "Custom read functions.
A hash that maps character to a function of two arguments,
stream (as a function) and character.  This function should
return the AST that has been read.  See usage of
`def-readclede-reader-syntax' later on.")

(defun clede-reader-make-stream (in)
  "IN string stream return the stream as a function of one optional argument.
When called with no arguments, this function should return the
next character from the stream.  When called with a non-nil
argument (character), this function should arrange that character
to be returned on next invokation with no arguments."
  (let ((unget nil))
    (when (symbolp in)
      (setq in (symbol-function in)))
    (let ((pos 0))
      (lambda (&optional ch)
	(cond
	 ((eq ch :pos)
	  (if (< pos (length in))
	      (- pos 1)
	    pos))
	 (ch (push ch unget))
	 (unget (pop unget))
	 ((< pos (length in))
	  (prog1 (aref in pos)
	    (setq pos (1+ pos)))))))))

(defun clede-reader-orig-read ()
  "Call the Emacs Lisp reader.
Both languages has sufficient structure similarities that there is
no need to ignore it."
  (funcall clede-reader-var-read '%clede-reader-readclede-reader-insym))

(defun clede-reader-peek (in)
  "Given IN stream function.
Return the next character without dropping it from the stream."
  (let ((ch (funcall in)))
    (funcall in ch)
    ch))

(defun clede-reader-next (in)
  "Return and discard the next character.
IN should be a stream function."
  (funcall in))

(defun clede-reader-read-while (in pred)
  "Read IN and return a string from the input stream.
As long as PRED the predicate--which will be called for each
charactclede-reader--returns true."
  (let ((chars (list)) ch)
    (while (and (setq ch (clede-reader-peek in))
                (funcall pred ch))
      (push (clede-reader-next in) chars))
    (apply #'string (nreverse chars))))

(defun clede-reader-croak (msg &rest args)
  "Error with message MSG out in case of parse error.
Using  ARGS as error args."
  (if args
      (apply #'error msg args)
    (error "%s" msg)))

(defun clede-reader-read-string ()
  "Read a string from the current stream.
It defers to `clede-reader-orig-read' and thus this should only
be called within the dynamic extent of some `read' function."
  (clede-reader-orig-read))

;;FIXME: Adapt it for a proper CL representation
(defun clede-reader-read-char ()
  "Read a character from the current stream.
It defers to `clede-reader-orig-read' and thus this should only
be called within the dynamic extent of some `read' function."
  (clede-reader-orig-read))

(defun clede-reader-skip-comment (in)
  "Skip over a comment (move to end of line).
Using IN as a stream."
  (clede-reader-read-while in (lambda (ch)
                      (not (eq ch ?\n)))))


(defun clede-reader-letter? (ch)
  "Test whether the given character CH is a Unicode letter."
  (memq (get-char-code-property ch 'general-category)
        '(Ll Lu Lo Lt Lm Mn Mc Me Nl)))

(defun clede-reader-whitespace? (ch)
  "Test if the given character(CH) is whitespace.
XXX actually not all Unicode whitespace chars are handled; I'm
not even sure that would be correct)."
  (memq ch '(?  ?\t ?\n ?\f ?\r #xa0)))

(defun clede-reader-digit? (ch)
  "Test if the given character(CH) is a plain digit."
  (<= ?0 ch ?9))

(defun clede-reader-number? (str)
  "Test if the given string(STR) should be interpreted as number."
  (string-match "^[-+]?\\(?:\\(?:[0-9]+\\|[0-9]*\\.[0-9]+\\)\\(?:[E|e][+|-]?[0-9]+\\)?\\)$" str))

(defun clede-reader-skip-whitespace (in)
  "Skip whitespace in the given stream(IN)."
  (clede-reader-read-while in #'clede-reader-whitespace?))

(defun clede-reader-read-symbol-name (in)
  "Read and return the name of a symbol in the stream(IN)."
  (clede-reader-read-while in (lambda (ch)
                      (cond
		       ((eq ch ?\\)
			(clede-reader-next in)
			(if (clede-reader-peek in) t (clede-reader-croak "Unterminated input")))
		       (t
			(or (clede-reader-letter? ch)
			    (clede-reader-digit? ch)
			    (memq ch '(?- ?+ ?= ?* ?/ ?_ ?~ ?! ?@ ?. ?\|
					  ?$ ?% ?^ ?& ?: ?< ?> ?{ ?} ?\?))))))))

(defun clede-reader-read-integer (in)
  "Read and return an integer.
NIL if there is no integer at current position in stream(IN))."
  (let ((num (clede-reader-read-while in #'clede-reader-digit?)))
    (when (< 0 (length num))
      (string-to-number num))))

(defun clede-reader-read-symbol (in)
  "Read a symbol or a number.
If what follows in the stream(IN) looks like a number, a number will
be returned (via the original reader).If a symbol, it might be
auto-prefixed if declared `local' in the current file."
  (let ((name (clede-reader-read-symbol-name in)))
    (cond
     ((clede-reader-number? name)
      (funcall clede-reader-var-read name))
     ((zerop (length name))
      '##)
     (t
      (intern name)))))


(defun clede-reader-read-list (in end &optional no-dot)
  "Read a list of elements from the input stream(IN) until END.
character has been observed.If NO-DOT is nil then it will
support a dot character before the last element, producing an
\"improper\" list. If NO-DOT is true, then if a single dot
character is encountered this will produce an error."
  (let ((ret nil) (p nil) ch)
    (catch 'exit
      (while t
        (clede-reader-skip-whitespace in)
        (setq ch (clede-reader-peek in))
        (cond
	 ((not ch)
	  (clede-reader-croak "Unterminated list"))
	 ((eq ch end)
	  (clede-reader-next in)
	  (throw 'exit ret))

	 (t
	  (let ((x (clede-reader-read-datum in)))
	    (cond
	     ((eq x '\.)
	      (cond
	       (no-dot (clede-reader-croak "Dot in wrong context"))
	       (t
		(rplacd p (clede-reader-read-datum in))
		(clede-reader-skip-whitespace in)
		(setq ch (clede-reader-next in))
		(unless (eq ch end)
		  (clede-reader-croak "Dot in wrong context"))
		(throw 'exit ret))))
	     (t
	      (let ((cell (cons x nil)))
		(setq p (if ret
			    (rplacd p cell)
			  (setq ret cell)))))))))))))

(defun clede-reader-read-datum (in)
  "Read and return a Lisp datum from the input stream(IN)."
  (clede-reader-skip-whitespace in)
  (let ((ch (clede-reader-peek in)) macrochar)
    (cond
     ((not ch)
      (clede-reader-croak "End of file during parsing"))
     ((eq ch ?\")
      (clede-reader-read-string))
     ((eq ch ?\;)
      (clede-reader-skip-comment in)
      (clede-reader-read-datum in))

     ;;FIXME: We need transform some characters #\a -> 7 and #\b -> 8
     ;; this is because the way the Emacs reader works with these symbols.
     ((eq ch #o7)
      (clede-reader-next in))
     ((eq ch #o10)
      (clede-reader-next in))
     ((eq ch ?\?)
      (clede-reader-read-char))
     ((eq ch ?\()
      (clede-reader-next in)
      (clede-reader-read-list in ?\)))
     ((eq ch ?\[)
      (clede-reader-next in)
      (apply #'vector (clede-reader-read-list in ?\] t)))
     ((eq ch ?\')
      (clede-reader-next in)
      (list 'quote (clede-reader-read-datum in)))
     ;;FIXME: For now, ignoring is not the worst
     ((eq ch ?\#)
      (clede-reader-next in)
      (clede-reader-read-datum in))
     ((eq ch ?\`)
      (clede-reader-next in)
      (list '\` (clede-reader-read-datum in)))
     ((eq ch ?\,)
      (clede-reader-next in)
      (cond
       ((eq (clede-reader-peek in) ?\@)
	(clede-reader-next in)
	(list '\,@ (clede-reader-read-datum in)))
       (t
	(list '\, (clede-reader-read-datum in)))))
     ((setq macrochar (gethash ch clede-reader-macro-chars))
      (clede-reader-next in)
      (funcall macrochar in ch))
     (t
      (clede-reader-read-symbol in)))))

(defvar *clede-reader-substitutions*)

(defun clede-reader-read-internal (in)
  "Read internal function from stream(IN)."
  (fset '%clede-reader-readclede-reader-insym in)
  (unwind-protect
      (let ((*clede-reader-substitutions* (list)))
	(clede-reader-read-datum in))
    (fset '%clede-reader-readclede-reader-insym nil)))

;; (defun def-read-clede-reader-syntax (ch reader)
;;   "Insert the reader as a read function."
;;   (puthash ch reader clede-reader-macro-chars))


(defun clede-reader-read (str)
  "Read a string(STR) and return the tokenize string."
  (let* ((stream (clede-reader-make-stream str))
         (token (clede-reader-read-internal stream)))
    token))

(provide 'clede-reader)
;;; clede-reader.el ends here
