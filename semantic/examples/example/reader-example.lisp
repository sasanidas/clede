;; Important reader differences between Common Lisp and Emacs lisp


;; Characters 

;;CL
#\a


;;EL
?a


;; Vector

;;CL
#(1 2)

;;EL
[1 2]

