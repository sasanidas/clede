;;; clede-semantic-util.el ---  CLEDE semantic utilities functions           -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 17 Mar 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/clede
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires
(require 'clede-semantic)

;; Get attribute
;;(semantic-tag-get-attribute tag :template)

(cl-defun clede-semantic-tag-point (&optional point)
  "Return the tag main point.
If POINT is non-nil, return the function from that point instead."
  (if point
      (save-excursion
	(goto-char point)
	(semantic-tag-start (semantic-current-tag)))
    (semantic-tag-start (semantic-current-tag))))

(cl-defun clede-semantic-get-tag (point)
  "Return the closer tag to POINT."
  (save-excursion
    (goto-char point)
    (semantic-current-tag)))


(cl-defun clede-semantic-util-add-type (symbol-type symbol-description)
  "Add a SYMBOL-TYPE to be indexed in imenu.
It uses SYMBOL-DESCRIPTION as a imenu label."
  (setq-mode-local lisp-mode semantic-symbol->name-assoc-list
		   (add-to-list 'semantic-symbol->name-assoc-list
				(cons symbol-type symbol-description))))


(cl-defun clede-semantic-util-tag-by-name (name &optional class)
  "Find and return the tag by NAME.
Optionally CLASS can be sent.
If there is one more tag with the same NAME, take the first."
  (seq-find (lambda (tag)
	      (if class
		  (and (equal (semantic-tag-class tag) class)
		       (string= (semantic-tag-name tag) name))
		(string= (semantic-tag-name tag) name)))
	    (semantic-fetch-tags)))


(cl-defun clede-semantic-util-tag-by-class (class)
  "Find and return a tag by CLASS."
  (seq-find (lambda (tag)
	      (equal (semantic-tag-class tag) class))
	    (semantic-fetch-tags)))


;;(start-let . end-let)
(cl-defun clede-semantic-util-get-let-context (current-symbol)
  "Get the current let context of the CURRENT-SYMBOL."
  (let (fn head-let tail-let is-in-let sexp-let)
    (save-excursion
      (while (and (not is-in-let)
		  (or
		   (car (setq sexp-let (save-excursion
					 (condition-case nil
					     (up-list -2)
					   (scan-error nil))
					 (cons (looking-at "((") (point)))))
		   (setq fn (semantic-ctxt-current-function-lisp-mode
			     (point) (list t)))))
	(when (car sexp-let)
	  (setq fn (cons "let" (cdr sexp-let))))
	(cond
	 ((eq (car fn) t)
	  nil)
	 ((member (car fn) '("let" "let*" "with-slots"))
	  (if (and (listp fn)
		   (cdr fn))
	      (goto-char (cdr fn)))
	  (up-list -1)
	  (setq head-let (point))
	  (forward-sexp)
	  (setq tail-let (point))
	  (goto-char head-let)
	  (forward-char 1)
	  (forward-symbol 1)
	  (skip-chars-forward "* \t\n")
	  (let ((varlst (clede-semantic-reader
			 :form-string (buffer-substring-no-properties
				       (point)
				       (save-excursion
					 (forward-sexp 1)
					 (point))))))
	    (mapc (lambda (lvar)
		    (when (string= (symbol-name (car lvar)) current-symbol)
		      (setq is-in-let t)))
		  varlst))))
	(up-list -1))
      (when is-in-let
	(cons head-let tail-let)))))


(provide 'clede-semantic-util)
;;; clede-semantic-util.el ends here
