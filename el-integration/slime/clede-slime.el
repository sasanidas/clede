;;; clede-slime.el ---  CLEDE slime integration           -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 17 Mar 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/clede
;; Package-Requires: ((emacs "26.3"))
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires

(cl-defun clede-slime-send-mrepl-command (command &optional process)
  "Send the COMMAND to the SLIME REPL.
If PROCESS is non-nil send there instead, PROCESS should have
a valid buffer associate with it."
  (let* (slime-con slime-buffer)
    (if process
	(setq slime-con process
	      slime-buffer (slime-repl-buffer t process))
      (setq slime-con (slime-connection)
	    slime-buffer (slime-repl-buffer t (slime-connection))))
    (with-current-buffer slime-buffer
      (save-excursion
	(goto-char (point-max))
	(insert command)
	(slime-repl-return))
      (goto-char (point-max)))))

(cl-defun clede-slime-send-sexp (sexp)
  "Send SEXP to the slime evaluator with `slime-eval'.
Get the return value."
  (let* ((return-val (slime-eval sexp)))
    return-val))


(provide 'clede-slime)
;;; clede-slime.el ends here
