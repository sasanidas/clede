;;; clede-inf-lisp.el ---  CLEDE inferior lisp integration           -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 17 Mar 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/clede
;; Package-Requires: ((emacs "26.3"))
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires
(require 'clede-semantic)

(defcustom clede-inf-lisp-buffer "*inferior-lisp*"
  "Default process to communicate with the inferior Lisp.
It takes the value from `inferior-lisp-buffer'."
  :group 'clede
  :type 'string)


(cl-defun clede-inf-lisp-send-command (command &optional process)
  "Send the COMMAND to the inferior REPL.
If PROCESS is non-nil send use this process instead"
  (let* (inf-process inf-buffer)
    (if process
	(setq inf-process process
	      inf-buffer (process-buffer inf-process))
      (setq inf-process (get-buffer-process clede-inf-lisp-buffer)
	    inf-buffer clede-inf-lisp-buffer))
    (with-current-buffer inf-buffer
      (save-excursion
	(goto-char (point-max))
	(insert command)
	(comint-send-input))
      (goto-char (point-max)))))

(cl-defun clede-inf-lisp--get-delete-output (process)
  "Return the output of PROCESS and delete it."
  (let ((proc process)
	(inhibit-read-only t)
	(output nil))
    (save-excursion
      (let ((pmark (progn (goto-char (process-mark proc))
			  (forward-line 0)
			  (point-marker))))
	(setq output (buffer-substring-no-properties
		      comint-last-input-end pmark))
	(delete-region comint-last-input-end pmark)
	(goto-char (process-mark proc))
	(goto-char (point-max))))
    ;; Output message and put back prompt
    (comint-output-filter proc "")
    output))

(cl-defun clede-inf-lisp-send-sexp (sexp)
  "Send SEXP to the inferior Lisp process in the buffer `clede-inf-lisp-buffer'.
Returning the evaluated value.
Keep in mind that if you make any global change, it will affect
the running Lisp process"
  (let* ((lisp-process (get-buffer-process clede-inf-lisp-buffer))
	 (lisp-buffer clede-inf-lisp-buffer))
    (with-current-buffer lisp-buffer
      (goto-char (point-max))
      (insert (format "%s" sexp))
      (comint-send-input)
      ;;TODO: Find a better way to wait until the comint buffer is ready
      (sleep-for 0.1)
      (ring-remove comint-input-ring 0)
      (prog1
	  (clede-semantic-reader :form-string (clede-inf-lisp--get-delete-output lisp-process))
	(forward-line -1)
	(goto-char (line-beginning-position))
	(kill-line)
	(kill-line)
	(kill-line)
	(goto-char (line-end-position))))))

(provide 'clede-inf-lisp)
;;; clede-inf-lisp.el ends here
