;;; clede-sly.el ---  CLEDE sly integration           -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 17 Mar 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/clede
;; Package-Requires: ((emacs "26.3"))
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires

(defun clede-sly-dialog-trace-current-function ()
  "Call slynk-trace-dialog with the current function/method."
  (interactive)
  (if (and (fboundp 'sly) (fboundp 'semantic-mode))
      (let* ((current-tag (semantic-current-tag))
	     (tag-name (cl-first current-tag))
	     (tag-class (cl-second current-tag)))
	(if (eq 'function tag-class)
	    (let* ((spec-string tag-name)
		   (spec-string (if (fboundp 'sly-trace-query)
				    (sly-trace-query spec-string)
				  spec-string)))
	      (sly-message "%s" (sly-eval `(slynk-trace-dialog:dialog-toggle-trace
					    (slynk::from-string ,spec-string))))
	      (run-hooks 'sly-trace-dialog-after-toggle-hook))
	  (error "The current tag %s is not of the function/method type" tag-name)))
    (error "Enable sly and semantic-mode to use this function")))

(cl-defun clede-sly-send-mrepl-command (command &optional process)
  "Send the COMMAND to the sly REPL.
If PROCESS is non-nil send there instead, PROCESS should have
a valid buffer associate with it."
  (let* (sly-con sly-buffer sly-process)
    (if process
	(setq sly-process process
	      sly-buffer (process-buffer sly-process))
      (setq sly-con sly-default-connection
	    sly-buffer (sly-buffer-name :mrepl :connection sly-con)
	    sly-process (get-buffer-process sly-buffer)))
    (with-current-buffer sly-buffer
      (save-excursion
	(goto-char (point-max))
	(insert command)
	(sly-mrepl-return))
      (goto-char (point-max)))))

(cl-defun clede-sly-send-sexp (sexp)
  "Send SEXP to the sly evaluator with `sly-eval'.
Get the return value."
  (let* ((return-val (sly-eval sexp)))
    return-val))


(provide 'clede-sly)
;;; clede-sly.el ends here
