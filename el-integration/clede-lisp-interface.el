;;; clede-lisp-interface.el ---  CLEDE CL agnostic interface           -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 17 Mar 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/clede
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires

(require 'clede)
(require 'clede-sly)
(require 'clede-slime)
(require 'clede-inf-lisp)


(defcustom clede-lisp-interface-package 'inf-lisp
  "Package to use for interaction with the Common Lisp REPL."
  :type 'symbol
  :options '(sly inf-lisp slime)
  :group 'clede)


(cl-defun clede-lisp-interface-send-repl-command (command &optional process)
  "Send COMMAND to the CL REPL.
Optionally PROCESS can be selected."
  (cond
   ((and (fboundp 'sly) (eq clede-lisp-interface-package 'sly))
    (clede-sly-send-mrepl-command command process))
   ((and (fboundp 'slime) (eq clede-lisp-interface-package 'slime))
    (clede-slime-send-mrepl-command command process))
   ((and (fboundp 'inferior-lisp) (eq clede-lisp-interface-package 'inf-lisp))
    (clede-inf-lisp-send-command command process))
   (t (error "No REPL found to send %s" command))))


(cl-defun clede-lisp-interface-send-sexp (sexp)
  "Send SEXP to the Lisp interface REPL.
Get the return value."
  (cond
   ((and (fboundp 'sly) (eq clede-lisp-interface-package 'sly))
    (clede-sly-send-sexp sexp))
   ((and (fboundp 'slime) (eq clede-lisp-interface-package 'slime))
    (clede-slime-send-sexp sexp))
   ((and (fboundp 'inferior-lisp) (eq clede-lisp-interface-package 'inf-lisp))
    (clede-inf-lisp-send-sexp sexp))
   (t (error "No REPL found to send the sexp %s" sexp))))



(provide 'clede-lisp-interface)
;;; clede-lisp-interface.el ends here
