;;; clede-fiveam.el ---  CLEDE fiveam integration           -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 17 Mar 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/clede
;; Package-Requires: ((emacs "26.3"))
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires

(require 'clede)
(require 'clede-semantic)
(require 'clede-semantic-util)
(require 'clede-lisp-interface)


(defcustom clede-fiveam-test-call "5am:run!"
  "Default function from the 5am package to call when launching a test."
  :group 'clede
  :type 'string)

(defsubst clede-fiveam-tag-new-test (name type &rest attributes)
  "Create a semantic tag of class `5amtest'.
NAME is the name of this variable.
TYPE is a string or semantic tag representing the type of this variable.
Optional DEFAULT-VALUE is a string representing the default value of this
variable.
ATTRIBUTES is a list of additional attributes belonging to this tag."
  (apply 'semantic-tag name '5amtest
         :type type
         attributes))

(defsubst clede-fiveam-tag-new-suite (name type &rest attributes)
  "Create a semantic tag of class `5amsuite'.
NAME is the name of this variable.
TYPE is a string or semantic tag representing the type of this variable.
Optional DEFAULT-VALUE is a string representing the default value of this
variable.
ATTRIBUTES is a list of additional attributes belonging to this tag."
  (apply 'semantic-tag name '5amsuite
         :type type
         attributes))

(defun clede-fiveam--parse-name (symbol)
  "Parse SYMBOL name."
  (let* ((symbol-n (if (listp symbol)
		       (symbol-name (car symbol))
		     (symbol-name symbol))))
    symbol-n))

(clede-semantic-lisp-setup-form-parser
 (lambda (form _start _end)
   (clede-fiveam-tag-new-test
    (clede-fiveam--parse-name (nth 1 form)) nil))

 5am:test
 fiveam:test
 test)

(clede-semantic-lisp-setup-form-parser
 (lambda (form _start _end)
   (clede-fiveam-tag-new-suite
    (symbol-name (nth 1 form)) nil))
 5am:in-suite
 fiveam:in-suite
 in-suite)

(clede-semantic-util-add-type '5amtest "Five Test")
(clede-semantic-util-add-type '5amsuite "Five Suite")


(cl-defun clede-fiveam-send-current-test ()
  "Send the current test to the CL REPL."
  (interactive)
  (let* ((test-name (semantic-tag-name (clede-semantic-get-tag (point))))
	 (test-call (format "(%s '%s)" clede-fiveam-test-call test-name)))
    (clede-lisp-interface-send-repl-command test-call)))

(cl-defun clede-fiveam-send-current-suite ()
  "Send the current test project suite."
  (interactive)
  (let* ((suite-name (semantic-tag-name (clede-semantic-util-tag-by-class '5amsuite)))
	 (suite-call (format "(%s '%s)" clede-fiveam-test-call suite-name)))
    (clede-lisp-interface-send-repl-command suite-call)))



(provide 'clede-fiveam)
;;; clede-fiveam.el ends here
