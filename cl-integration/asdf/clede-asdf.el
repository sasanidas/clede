;;; clede-asdf.el ---  CLEDE asdf integration           -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 17 Mar 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/clede
;; Package-Requires: ((emacs "26.3"))
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires

(require 'clede)
(require 'clede-semantic)
(require 'clede-lisp-interface)


;;TODO: Extract customizable variables

(defsubst clede-asdf-tag-system (name type &rest attributes)
  "Create a semantic tag of class `defsystem'.
NAME is the name of this variable.
TYPE is a string or semantic tag representing the type of this variable.
Optional DEFAULT-VALUE is a string representing the default value of this
variable.
ATTRIBUTES is a list of additional attributes belonging to this tag."
  (apply 'semantic-tag name 'system
         :type type
         attributes))

(cl-defun clede-asdf-parse-components (components)
  "Parse COMPONENTS to a folder/file plist."
  (let* ((asdf-location-files)
	 (output (cl-loop for component in components
			  for module = (cl-getf component :module)
			  for files = (cl-getf component :components)
			  for pathname = (cl-getf component :pathname)
			  for file = (cl-getf component :file)
			  if (and module files)
			  collect `(:folder ,(if pathname pathname module) :files ,(mapcan #'cdr files))
			  else do (cl-pushnew  file asdf-location-files))))
    (cond
     ((and output asdf-location-files)
      (cl-pushnew `(:folder "." :files ,asdf-location-files ) output))
     ((and output (null asdf-location-files))
      output)
     ((and (null output) asdf-location-files)
      (list `(:folder "." :files ,asdf-location-files ))))))

(clede-semantic-lisp-setup-form-parser
 (lambda (form _start _end)
   (let ((attr-plist (cl-subseq form 2)))
     (clede-asdf-tag-system
      ;; The system maybe a keyword
      (format "%s" (cl-second form))
      nil
      :version (cl-getf attr-plist :version)
      :author (cl-getf attr-plist :author)
      :license (cl-getf attr-plist :license)
      :depends-on (cl-getf attr-plist :depends-on)
      :components (clede-asdf-parse-components (cl-getf attr-plist :components))
      :description (cl-getf attr-plist :description))))
 defsystem)


(cl-defun clede-asdf-go-component-file ()
  "Go to the component file."
  (interactive)
  (let* ((system-components
	  (semantic-tag-get-attribute (semantic-current-tag) :components))
	 ;;FIXME: Adapt the semantic tag to save the location of the file, instead
	 ;; of enforcing the current file
	 (system-location (file-name-directory (buffer-file-name)))
	 (folders (mapcar (lambda (folder) (cl-getf folder :folder)) system-components))
	 (selected-folder (completing-read "Select the folder: " folders))
	 (selected-file (completing-read "Select destination file: "
					 (cl-getf
					  (cl-find selected-folder system-components
						   :key #'cl-second :test #'string-equal) :files))))
    (find-file(expand-file-name (format "%s%s" selected-file ".lisp")
				(expand-file-name selected-folder system-location) ))))



(cl-defun clede-asdf--get-system-def-file (cl-system-name)
  "Get the system asd file from CL-SYSTEM-NAME."
  (when (eq clede-lisp-interface-package 'inf-lisp)
    (setq cl-system-name (format "%s%s" ":" cl-system-name)))
  (clede-lisp-interface-send-sexp
   `(cl:namestring (asdf:system-source-file ,cl-system-name))))

(cl-defun clede-asdf-go-def-file ()
  "Go to a system definition .asd file.
The system list is taken from the asdf function asdf:already-loaded-systems."
  (interactive)
  (let* ((asdf-systems (clede-lisp-interface-send-sexp '(asdf:already-loaded-systems)))
	 (asdf-selected (completing-read "Select asdf system: "
					 asdf-systems))
	 (asd-file (clede-asdf--get-system-def-file asdf-selected)))
    (find-file asd-file)))

;;TODO: Search in the asdf:*central-registry* for auto-completion
(cl-defun clede-asdf-load-system (cl-system-name)
  "Load the system with the name CL-SYSTEM-NAME."
  (interactive "sSystem name: ")
  (clede-lisp-interface-send-repl-command (format "(asdf:load-system :%s)" cl-system-name)))

(provide 'clede-asdf)
;;; clede-asdf.el ends here
