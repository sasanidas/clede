;;; clede.el ---  CLEDE (Common Lisp -E-macs Development Environment)           -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz
;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; Created: 17 Mar 2021
;; Version: 0.0.1
;; Keywords: languages
;; URL: https://gitlab.com/sasanidas/clede
;; Package-Requires: ((emacs "26.3"))
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro
;;

;;; Code:

;;;; The requires

(eval-when-compile
  (require 'cl-lib))


(defgroup clede nil
  "CLEDE utilities group."
  :prefix "clede-"
  :tag "CLEDE"
  :group 'utilities
  :link '(url-link :tag "Repository" "https://gitlab.com/sasanidas/clede"))


;;(defcustom clede-libraries-integrations nil
(defcustom clede-integrations '(clede-fiveam clede-asdf clede-refactor clede-highlight clede-commands)
  "CLEDE integration with Common Lisp libraries.
The integrations are described in the folder 'cl-integration'."
  :group 'clede
  :type 'list)

(defvar clede-minor-mode-map (make-sparse-keymap)
  "The keymap for `clede-minor-mode'.")

;; (if clede-minor-mode-map
;;     nil
;;   (let ((map (make-sparse-keymap)))
;;     (define-key map (kbd "C-c t") 'find-file)
;;     (setq clede-minor-mode-map map)))


;; Add subdirectories to the load-path
(let ((default-directory
	(if load-file-name
	    (file-name-directory load-file-name)
	  (file-name-directory (buffer-file-name)))))
  (normal-top-level-add-subdirs-to-load-path))

(define-minor-mode clede-minor-mode
  "Toggle CLEDE minor mode.
With no argument, this command toggles the mode.
Non-null prefix argument turns on the mode.
Null prefix argument turns off the mode."
  :lighter " CLEDE"
  :group 'clede
  :keymap clede-minor-mode-map)

(cl-defun clede-start ()
  "Initialize CLEDE."
  (interactive)
  (when clede-integrations
    (mapcar #'require clede-integrations)))



(provide 'clede)
;;; clede.el ends here
